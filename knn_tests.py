import numpy as np
from matplotlib import pyplot as plt
import pandas as pd

df = pd.DataFrame({
    'x': np.random.randint(0, 100, 50),
    'y': np.random.randint(0, 100, 50)
})

# print(df['x'][1])
# print(df['x'])
centers = {
    i+1: [np.random.randint(0, 80), np.random.randint(0, 80)]
    for i in range(6)
}

# print(centers[1])
# print(centers[1][0])

for i in centers.keys():
    # sqrt((x1 - x2)^2 - (y1 - y2)^2)
    df['distance_from_center_{}'.format(i)] = (
        np.sqrt(
            (df['x'] - centers[i][0]) ** 2
            + (df['y'] - centers[i][1]) ** 2
        )
    )

print(df)