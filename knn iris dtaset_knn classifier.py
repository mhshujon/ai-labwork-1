import sklearn
from sklearn.utils import shuffle
from sklearn.datasets import load_iris
import pandas as pd
import numpy as np
from sklearn import linear_model, preprocessing
import math

data = load_iris()
print(data)

shuffled_data = shuffle(data.data)
print(shuffled_data)

le = preprocessing.LabelEncoder()

sepal_length = le.fit_transform(list(data["sepal_length"]))
sepal_width = le.fit_transform(list(data["sepal_width"]))
petel_length = le.fit_transform(list(data["petel_length"]))
petel_width = le.fit_transform(list(data["petel_width"]))
variety = le.fit_transform(list(data["variety"]))

# S = 0
# X = list(zip(sepal_length, sepal_width, petel_length, petel_width))  # features
# y = list(variety)  # labels
#
# x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(X, y, test_size = 0.1)
#
# #model = KNeighborsClassifier(n_neighbors=11)
#
# #model.fit(x_train, y_train)
#
# for x in X.x():
#     S += math.pow(X[x] - y[x], 2)
# acc = S.score(x_test, y_test)
# print(acc)
#
# predicted = S.predict(x_test)
# names = ["Iris-setosa", "Iris-versicolor", "Iris-virginica"]
#
# for x in range(len(predicted)):
#     print("Predicted: ", names[predicted[x]], "Data: ", x_test[x], "Actual: ", names[y_test[x]])
#     # Now we will we see the neighbors of each point in our testing data
#     n = S.kneighbors([x_test[x]], 11, True)
#     print("N: ", n)