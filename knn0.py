import csv
import random
import math
import operator
import numpy as np
from sklearn.utils import shuffle


def loadDataset(filename, split, trainingSet=[], testSet=[]):
    with open(filename, 'r') as csvfile:
        lines = csv.reader(csvfile)
        datasets = list(lines)
        print(datasets)
        array = np.array(datasets)
        # print(array)
        shuffled = shuffle(array)
        # print(shuffled)
        dataset = list(shuffled)
        print(dataset)

        # dataset = shuffle(shuffled)
        # for x in range(len(dataset) - 1):
        #     for y in range(4):
        #         dataset[x][y] = float(dataset[x][y])
        #     if random.random() < split:
        #         trainingSet.append(dataset[x])
        #     else:
        #         testSet.append(dataset[x])


def main():
    # prepare data
    trainingSet = []
    testSet = []
    split = 0.67
    loadDataset('iris.data', split, trainingSet, testSet)
    print('Train set: ' + repr(len(trainingSet)))
    print('Test set: ' + repr(len(testSet)))
    # generate predictions
    # predictions = []
    # k = 3
    # for x in range(len(testSet)):
    #     neighbors = getNeighbors(trainingSet, testSet[x], k)
    #     result = getResponse(neighbors)
    #     predictions.append(result)
    #     print('> predicted=' + repr(result) + ', actual=' + repr(testSet[x][-1]))
    # accuracy = getAccuracy(testSet, predictions)
    # print('Accuracy: ' + repr(accuracy) + '%')

main()