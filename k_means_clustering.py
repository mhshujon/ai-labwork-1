import numpy as np
from matplotlib import pyplot as plt
import pandas as pd
import copy

k = 6

dataframe = pd.DataFrame({
    'x': np.random.randint(0, 100, 50),
    'y': np.random.randint(0, 100, 50)
})

centers = {
    i+1: [np.random.randint(0, 100), np.random.randint(0, 100)]
    for i in range(k)
}

colormap = {1: 'red', 2: 'green', 3: 'blue', 4: 'purple', 5: 'gray', 6: 'chocolate'}

plt.scatter(dataframe['x'], dataframe['y'], color='k', alpha=0.5, edgecolor='k')

for x in centers.keys():
    plt.scatter(centers[x][0], centers[x][1], marker='*', c=colormap[x], s=300)

plt.show()


# Assignment Stage

def assignment(dataframe, centers):
    for i in centers.keys():
        # sqrt((x1 - x2)^2 - (y1 - y2)^2)
        dataframe['distance_from_center{}'.format(i)] = (
            np.sqrt(
                (dataframe['x'] - centers[i][0]) ** 2
                + (dataframe['y'] - centers[i][1]) ** 2
            )
        )
    center_distance_cols = ['distance_from_center{}'.format(i) for i in centers.keys()]
    dataframe['closest'] = dataframe.loc[:, center_distance_cols].idxmin(axis=1)
    dataframe['closest'] = dataframe['closest'].map(lambda x: int(x.lstrip('distance_from_')))
    dataframe['color'] = dataframe['closest'].map(lambda x: colormap[x])
    return dataframe

dataframe = assignment(dataframe, centers)
# print(dataframe.head())

plt.scatter(dataframe['x'], dataframe['y'], color=dataframe['color'], alpha=0.5, edgecolor='k')

for i in centers.keys():
    plt.scatter(centers[i][0], centers[i][1], marker='*', color=colormap[i], s=300)

plt.show()


## Update Stage

old_centers = copy.deepcopy(centers)

def update(centers):
    for i in centers.keys():
        centers[i][0] = np.mean(dataframe[dataframe['closest'] == i]['x'])
        centers[i][1] = np.mean(dataframe[dataframe['closest'] == i]['y'])
    return centers

centers = update(centers)

ax = plt.axes()
plt.scatter(dataframe['x'], dataframe['y'], color=dataframe['color'], alpha=0.5, edgecolor='k')
for i in centers.keys():
    plt.scatter(centers[i][0], centers[i][1], marker='*', color=colormap[i], s=300)

for i in old_centers.keys():
    old_x = old_centers[i][0]
    old_y = old_centers[i][1]
    dx = (centers[i][0] - old_centers[i][0]) * 0.75
    dy = (centers[i][1] - old_centers[i][1]) * 0.75
    ax.arrow(old_x, old_y, dx, dy, head_width=2, head_length=3, fc=colormap[i], ec=colormap[i])
plt.show()

# Continue until all assigned categories don't change any more
while True:
    closest_centers = dataframe['closest'].copy(deep=True)
    centers = update(centers)
    dataframe = assignment(dataframe, centers)
    if closest_centers.equals(dataframe['closest']):
        break

plt.scatter(dataframe['x'], dataframe['y'], color=dataframe['color'], alpha=0.5, edgecolor='k')

for i in centers.keys():
    plt.scatter(centers[i][0], centers[i][1], marker='*', color=colormap[i], s=300)

plt.show()